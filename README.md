# CStructures
simple implementation of different data structures in C++.

- [x] Linked list
- [ ] Queue
- [ ] Stack
- [x] Binary search tree
- [ ] Priority Queue
- [ ] Hash tables
- [x] Ordered map
- [x] Ordered set
- [x] Immutable List
