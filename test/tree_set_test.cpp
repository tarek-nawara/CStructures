/*
 * File: tree_set_test.cpp
 * Description: holder for testing of ordered set
 * Created at: 2018-4-18
 */

#include "../src/set/tree_set.hpp"

#include <functional>

#include "gtest/gtest.h"

TEST(TreeSet, Creation) {
    TreeSet<int> set{1, 2, 3};
    EXPECT_EQ(set.Contains(1), true);
}

TEST(TreeSet, Addition) {
    constexpr int LEN = 100;
    TreeSet<int> set{};
    for (int i = 0; i < LEN; ++i) {
        set.Add(1);
    }
    EXPECT_EQ(set.Size(), 1);
    EXPECT_EQ(set.Contains(1), true);
}
