/*
 * File: linked_list_test.cpp
 * Description: test cases for linked list implementation
 * Created at: 2018-4-12
 */

#include "../src/list/linked_list.hpp"

#include <functional>

#include "gtest/gtest.h"

TEST(LinkedList, EmptyList) {
    LinkedList<int> list{};
    EXPECT_EQ(list.Empty(), true);
}

TEST(LinkedList, PushElement) {
    LinkedList<int> list{};
    list.Push(1);
    list.Push(2);

    EXPECT_EQ(list.Size(), 2);
    EXPECT_EQ(list.Get(0), 1);
    EXPECT_EQ(list.Get(1), 2);
}

TEST(LinkedList, Pop) {
    LinkedList<int> list{1, 2, 3};
    auto i = list.Pop();

    EXPECT_EQ(i, 3);
    EXPECT_EQ(list.Size(), 2);
}

TEST(LinkedList, Back) {
    LinkedList<int> list{1, 2, 3};
    EXPECT_EQ(3, list.Back());
}

TEST(LinkedList, CopyConstructor) {
    LinkedList<int> list{1, 2, 3};
    LinkedList<int> other = list;
    ++list[1];
    EXPECT_EQ(other[1], 2) << "list element is = " << list.Get(1)
                           << "other = " << other.Get(1);
    EXPECT_EQ(list[1], 3);
}

TEST(LinkedList, Reverse) {
    LinkedList<int> list{1, 2, 3};
    list.Reverse();
    EXPECT_EQ(list[0], 3);
    EXPECT_EQ(list[1], 2);
    EXPECT_EQ(list[2], 1);
}

TEST(LinkedList, RemoveAt) {
    LinkedList<int> list{1, 2, 3, 4};
    list.RemoveAt(2);

    EXPECT_EQ(list.Size(), 3);
    EXPECT_EQ(list[1], 2);
    EXPECT_EQ(list[2], 4);
}

TEST(LinkedList, Remove) {
    LinkedList<int> list{1, 2, 3, 4};
    std::function<bool(const int &, const int &)> comp =
        [](const int &x, const int &y) -> bool { return x == y; };
    list.Remove(1, comp);
    EXPECT_EQ(list.Size(), 3);
    EXPECT_EQ(list[0], 2);
}

TEST(LinkedList, Map) {
    LinkedList<int> list{1, 2, 3, 4};
    std::function<int(const int &)> mapper = [](int x) -> int { return x * x; };
    auto mapped_list = list.Map(mapper);
    for (size_t i = 0; i < mapped_list.Size(); ++i) {
        EXPECT_EQ(mapped_list[i], (i + 1) * (i + 1));
    }
}

TEST(LinkedList, Filter) {
    LinkedList<int> list{1, 2, 3, 4};
    std::function<bool(const int &)> predicate = [](int x) -> bool {
        return (x & 1) == 0;
    };
    auto even_list = list.Filter(predicate);
    EXPECT_EQ(even_list.Size(), 2);
    EXPECT_EQ(even_list[0], 2);
    EXPECT_EQ(even_list[1], 4);
}
