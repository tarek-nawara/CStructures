/*
 * File: tree_map_test.hpp
 * Description: holder for testing the tree map implementation
 * Created at: 2018-4-15
 */

#include "../src/map/tree_map.hpp"

#include <functional>

#include "gtest/gtest.h"

TEST(TreeMap, Creation) {
    TreeMap<int, int> map{{1, 1}, {2, 2}};
    EXPECT_EQ(true, true);
}
