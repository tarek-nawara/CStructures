/*
 * File: bst_test.cpp
 * Description: holder for testing of binary search tree
 * Created at: 2018-4-13
 */

#include "../src/bst/bst.hpp"

#include <functional>

#include "gtest/gtest.h"

TEST(BstCreation, BasicAssertions) {
    Bst<int, int> bst{{1, 1}, {2, 2}, {3, 3}};
    EXPECT_EQ(bst.Size(), 3);
}

TEST(Bst, Contains) {
    Bst<int, int> bst{{1, 1}, {2, 2}, {3, 3}};
    EXPECT_EQ(bst.Contains(1), true);
    EXPECT_EQ(bst.Contains(2), true);
    EXPECT_EQ(bst.Contains(3), true);
    EXPECT_EQ(bst.Contains(4), false);
}

TEST(Bst, Indexing) {
    Bst<int, int> bst{{1, 1}, {2, 2}, {3, 3}};
    EXPECT_EQ(bst[1], 1);
}

TEST(Bst, Removing) {
    Bst<int, int> bst{{1, 1}, {2, 2}, {3, 3}};
    bst.Remove(1);
    EXPECT_EQ(bst.Contains(1), false);
    EXPECT_EQ(bst.Size(), 2);
    EXPECT_EQ(bst.Contains(2), true);
    EXPECT_EQ(bst.Contains(3), true);
}

TEST(Bst, HugeAddition) {
    constexpr int LEN = 1000;
    Bst<int, int> bst{};
    for (int i = 0; i < LEN; ++i) {
        bst.Add(i, i);
    }
    std::cerr << "Bst Height=" << bst.Height() << '\n';
    for (int i = 0; i < LEN; ++i) {
        EXPECT_EQ(bst.Contains(i), true);
    }
}

TEST(Bst, HugeAdditionAndRemoval) {
    constexpr int LEN = 1000;
    Bst<int, int> bst{};
    for (int i = 0; i < LEN; ++i) {
        bst.Add(i, i);
    }
    for (int i = 0; i < LEN; i += 2) {
        bst.Remove(i);
    }
    std::cerr << "Bst Height=" << bst.Height() << '\n';
    for (int i = 1; i < LEN; i += 2) {
        EXPECT_EQ(bst.Contains(i), true);
    }
}
