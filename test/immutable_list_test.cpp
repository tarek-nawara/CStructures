/*
 * File: immutable_list.cpp
 * Description: test cases for immutable list implementation
 * Created at: 2019-3-16
 */

#include <functional>

#include "../src/immutable_list/immutable_list_node.hpp"
#include "gtest/gtest.h"

TEST(Empty, Add) {
    ListNode<int> *list = new Empty<int>();
    list = list->Add(1);
    EXPECT_EQ(list->Length(), 1);
    EXPECT_EQ(list->Get(0), 1);
}

TEST(Cons, Map) {
    ListNode<int> *list = new Empty<int>();
    list = list->Add(1);
    list = list->Add(2);
    std::function<int(int)> mapper = [](int x) { return 2 * x; };
    ListNode<int> *other = ListNodeUtils::Map(list, mapper);
    EXPECT_EQ(other->Length(), 2);
    EXPECT_EQ(other->Get(0), 4);
    delete list;
    delete other;
}

TEST(Cons, Filter) {
    ListNode<int> *list = new Empty<int>();
    list = list->Add(1);
    list = list->Add(2);
    std::function<bool(int)> predicate = [](int x) { return x % 2 == 0; };
    ListNode<int> *other = ListNodeUtils::Filter(list, predicate);
    EXPECT_EQ(other->Length(), 1);
    EXPECT_EQ(other->Get(0), 2);
    delete list;
    delete other;
}
