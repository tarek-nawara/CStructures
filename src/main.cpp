#include <iostream>

#include "list/linked_list.hpp"
#include "bst/bst.hpp"

int main() {
    LinkedList<int> list{1, 2, 3};
    Bst<int, int> bst{{1, 2}, {3, 4}};
    std::cout << list.Size() << '\n';

    auto other = list;
    ++other[1];
    std::cout << other << '\n';

    other.Reverse();
    std::cout << other << '\n';

    std::cout << bst << '\n';

    return 0;
}
