/*
 * File: bst_node.hpp
 * Description: holder for bst node implementation
 * Created at: 2018-4-13
 */

#ifndef _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_BST_BST_NODE_HPP
#define _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_BST_BST_NODE_HPP

#include <functional>
#include <iostream>

template <typename K, typename V>
class BstNode {
   public:
    BstNode(K key, V value);
    BstNode(K key, V value, BstNode<K, V>* left, BstNode<K, V>* right);

    K key;
    V value;
    BstNode<K, V>* left = nullptr;
    BstNode<K, V>* right = nullptr;
    std::size_t height = 0;
};

template <typename K, typename V>
BstNode<K, V>::BstNode(K key, V value) : key(key), value(value) {}

template <typename K, typename V>
BstNode<K, V>::BstNode(K key, V value, BstNode<K, V>* left,
                       BstNode<K, V>* right)
    : key(key), value(value), left(left), right(right) {}

#endif  // _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_BST_BST_NODE_HPP
