/**
 * File: bst.hpp
 * Description: holder for implementation of binary search tree.
 * Created at: 2018-4-13
 */

#ifndef _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_BST_BST_HPP
#define _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_BST_BST_HPP

#include <vector>
#include "bst_node.hpp"

template <typename K, typename V>
class Bst {
   protected:
    using Comp = std::function<int(const K&, const K&)>;
    using InitList = std::initializer_list<std::pair<K, V>>;

   public:
    Bst();
    explicit Bst(Comp& comp);
    Bst(InitList init_list);
    Bst(Comp& comp, InitList init_list);
    Bst(Bst<K, V>& tree);
    ~Bst();
    void Add(K key, V value);
    void Remove(K key);
    auto Size() -> std::size_t;
    auto Contains(const K& key) -> bool;
    auto operator[](const K& key) -> V&;
    auto Height() -> int;
    auto Empty() -> bool;
    auto EntrySet() -> std::vector<std::pair<K, V>>;

    template <typename FK, typename FV>
    friend auto operator<<(std::ostream& os, Bst<FK, FV> root) -> std::ostream&;

   private:
    BstNode<K, V>* root_ = nullptr;
    std::size_t size_ = 0;
    Comp comp_ = [](const K& a, const K& b) -> int {
        if (a == b) {
            return 0;
        }
        if (a < b) {
            return -1;
        } else {
            return 1;
        }
    };

    auto Add(BstNode<K, V>* root, K key, V value) -> BstNode<K, V>*;
    auto Remove(BstNode<K, V>* root, K key) -> BstNode<K, V>*;
    auto Contains(BstNode<K, V>* root, const K& key) -> bool;
    auto Get(BstNode<K, V>* root, const K& key) -> BstNode<K, V>*;
    auto FindMin(BstNode<K, V>* root) -> BstNode<K, V>*;
    void DeleteTree(BstNode<K, V>* root);
    void ToVector(BstNode<K, V>* root, std::vector<std::pair<K, V>>& res);
    auto AdjustTree(BstNode<K, V>* root) -> BstNode<K, V>*;
    auto RotateRight(BstNode<K, V>* root) -> BstNode<K, V>*;
    auto RotateLeft(BstNode<K, V>* root) -> BstNode<K, V>*;
    void AdjustHeight(BstNode<K, V>* root);
    auto GetHeight(BstNode<K, V>* node) -> std::size_t;
};

template <typename K, typename V>
Bst<K, V>::Bst() = default;

template <typename K, typename V>
Bst<K, V>::Bst(Comp& comp) : comp_(comp) {}

template <typename K, typename V>
Bst<K, V>::Bst(InitList init_list) {
    for (auto& i : init_list) {
        Add(i.first, i.second);
    }
}

template <typename K, typename V>
Bst<K, V>::Bst(Comp& comp, InitList init_list) {
    if (comp != nullptr) {
        comp_ = comp;
    }
    for (auto& i : init_list) {
        Add(i.first, i.second);
    }
}

template <typename K, typename V>
Bst<K, V>::Bst(Bst<K, V>& tree) : comp_(tree.comp_) {
    auto elems = tree.EntrySet();
    for (auto i : elems) {
        Add(i.first, i.second);
    }
}

template <typename K, typename V>
Bst<K, V>::~Bst() {
    DeleteTree(root_);
}

template <typename K, typename V>
void Bst<K, V>::Add(K key, V value) {
    if (Contains(key)) {
        auto node = Get(root_, key);
        node->value = value;
        return;
    }
    ++size_;
    root_ = Add(root_, key, value);
}

template <typename K, typename V>
void Bst<K, V>::Remove(K key) {
    if (!Contains(key)) {
        return;
    }
    --size_;
    root_ = Remove(root_, key);
}

template <typename K, typename V>
auto Bst<K, V>::Size() -> std::size_t {
    return size_;
}

template <typename K, typename V>
auto operator<<(std::ostream& os, Bst<K, V> root) -> std::ostream& {
    auto res = root.EntrySet();
    os << '[';
    for (size_t i = 0; i < res.size(); ++i) {
        auto& [k, v] = res[i];
        if (i > 0) {
            os << ',';
        }
        os << '(' << k << "," << v << ')';
    }
    os << ']';
    os << '\n';
    return os;
}

template <typename K, typename V>
auto Bst<K, V>::Contains(const K& key) -> bool {
    return Contains(root_, key);
}

template <typename K, typename V>
auto Bst<K, V>::operator[](const K& key) -> V& {
    auto res = Get(root_, key);
    if (res != nullptr) {
        return res->value;
    }
    throw std::invalid_argument("key not found");
}

template <typename K, typename V>
auto Bst<K, V>::EntrySet() -> std::vector<std::pair<K, V>> {
    std::vector<std::pair<K, V>> res{};
    ToVector(root_, res);
    return res;
}

template <typename K, typename V>
auto Bst<K, V>::Height() -> int {
    if (root_ == nullptr) {
        return -1;
    }
    return static_cast<int>(root_->height);
}

template <typename K, typename V>
auto Bst<K, V>::Empty() -> bool {
    return size_ == 0;
}

template <typename K, typename V>
auto Bst<K, V>::Add(BstNode<K, V>* root, K key, V value) -> BstNode<K, V>* {
    if (root == nullptr) {
        root = new BstNode<K, V>(key, value);
        return root;
    }
    int cmp = comp_(root->key, key);
    if (cmp < 0) {
        root->right = Add(root->right, key, value);
    } else if (cmp > 0) {
        root->left = Add(root->left, key, value);
    }
    root = AdjustTree(root);
    return root;
}

template <typename K, typename V>
auto Bst<K, V>::Remove(BstNode<K, V>* root, K key) -> BstNode<K, V>* {
    if (root == nullptr) {
        return nullptr;
    }
    int cmp = comp_(root->key, key);
    if (cmp < 0) {
        root->right = Remove(root->right, key);
    } else if (cmp > 0) {
        root->left = Remove(root->left, key);
    } else {
        if (root->left == nullptr && root->right == nullptr) {
            delete root;
            return nullptr;
        }
        if (root->left == nullptr) {
            auto old = root;
            root = root->right;
            delete old;
        } else if (root->right == nullptr) {
            auto old = root;
            root = root->left;
            delete old;
        } else {
            auto min_right = FindMin(root->right);
            std::cerr << "min_right" << min_right->key << '\n';
            root->key = min_right->key;
            root->value = min_right->value;
            root->right = Remove(root->right, min_right->key);
        }
        root = AdjustTree(root);
    }
    return root;
}

template <typename K, typename V>
auto Bst<K, V>::Contains(BstNode<K, V>* root, const K& key) -> bool {
    if (root == nullptr) {
        return false;
    }
    int cmp = comp_(root->key, key);
    if (cmp < 0) {
        return Contains(root->right, key);
    }
    if (cmp > 0) {
        return Contains(root->left, key);
    }
    return true;
}

template <typename K, typename V>
auto Bst<K, V>::Get(BstNode<K, V>* root, const K& key) -> BstNode<K, V>* {
    if (root == nullptr) {
        return nullptr;
    }
    int cmp = comp_(root->key, key);
    if (cmp < 0) {
        return Get(root->right, key);
    }
    if (cmp > 0) {
        return Get(root->left, key);
    }
    return root;
}

template <typename K, typename V>
auto Bst<K, V>::FindMin(BstNode<K, V>* root) -> BstNode<K, V>* {
    while (root->left != nullptr) {
        root = root->left;
    }
    return root;
}

template <typename K, typename V>
void Bst<K, V>::DeleteTree(BstNode<K, V>* root) {
    if (root == nullptr) {
        return;
    }
    DeleteTree(root->left);
    DeleteTree(root->right);
    delete root;
}

template <typename K, typename V>
void Bst<K, V>::ToVector(BstNode<K, V>* root,
                         std::vector<std::pair<K, V>>& res) {
    if (root == nullptr) {
        return;
    }
    ToVector(root->left, res);
    res.push_back({root->key, root->value});
    ToVector(root->right, res);
}

template <typename K, typename V>
auto Bst<K, V>::AdjustTree(BstNode<K, V>* root) -> BstNode<K, V>* {
    if (root == nullptr) {
        return nullptr;
    }
    auto left_height = GetHeight(root->left);
    auto right_height = GetHeight(root->right);
    long balance = left_height - right_height;
    if (balance > 1) {
        if (GetHeight(root->left->left) >= GetHeight(root->left->right)) {
            root = RotateRight(root);
        } else {
            root->left = RotateLeft(root->left);
            root = RotateRight(root);
        }
    } else if (balance < -1) {
        if (GetHeight(root->right->right) >= GetHeight(root->right->left)) {
            root = RotateLeft(root);
        } else {
            root->right = RotateRight(root->right);
            root = RotateLeft(root);
        }
    }
    AdjustHeight(root);
    return root;
}

template <typename K, typename V>
auto Bst<K, V>::RotateLeft(BstNode<K, V>* root) -> BstNode<K, V>* {
    auto new_root = root->right;
    root->right = new_root->left;
    new_root->left = root;
    AdjustHeight(root);
    AdjustHeight(new_root);
    return new_root;
}

template <typename K, typename V>
auto Bst<K, V>::RotateRight(BstNode<K, V>* root) -> BstNode<K, V>* {
    auto new_root = root->left;
    root->left = new_root->right;
    new_root->right = root;
    AdjustHeight(root);
    AdjustHeight(new_root);
    return new_root;
}

template <typename K, typename V>
void Bst<K, V>::AdjustHeight(BstNode<K, V>* root) {
    auto left_height = GetHeight(root->left);
    auto right_height = GetHeight(root->right);
    root->height = 1 + std::max(left_height, right_height);
}

template <typename K, typename V>
auto Bst<K, V>::GetHeight(BstNode<K, V>* node) -> std::size_t {
    if (node == nullptr) {
        return 0;
    }
    return node->height;
}

#endif  // _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_BST_BST_HPP
