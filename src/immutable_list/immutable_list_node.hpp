/**
 * File: immutable_list_node.hpp
 * Description: holds the definition of the list node and
 *              its subclasses
 * Created at: 2019-3-16
 */

#ifndef _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_IMMUTABLE_LIST_IMMUTABLE_LIST_NODE_HPP
#define _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_IMMUTABLE_LIST_IMMUTABLE_LIST_NODE_HPP

#include <algorithm>

enum class ListType { EMPTY, CONS };

template <typename T>
class ListNode {
   public:
    virtual ~ListNode() = default;
    virtual auto Add(T data) -> ListNode<T>* = 0;
    virtual auto Remove(T data) -> ListNode<T>* = 0;
    virtual auto Length() -> std::size_t = 0;
    virtual auto Contains(T data) -> bool = 0;
    virtual auto Get(int index) -> T = 0;
    virtual auto GetHead() -> T = 0;
    virtual auto GetTail() -> ListNode<T>* = 0;
    virtual auto GetType() -> ListType = 0;
};

template <typename T>
class Cons : public ListNode<T> {
   public:
    ~Cons() { delete tail_; }
    Cons(T head, ListNode<T>* tail) : head_(head), tail_(tail) {}
    auto Add(T data) -> ListNode<T>* override;
    auto Remove(T data) -> ListNode<T>* override;
    auto Length() -> std::size_t override;
    auto Contains(T data) -> bool override;
    auto Get(int index) -> T override;
    auto GetHead() -> T override;
    auto GetTail() -> ListNode<T>* override;

    auto GetType() -> ListType override { return ListType::CONS; }

   private:
    T head_;
    ListNode<T>* tail_;
};

template <typename T>
auto Cons<T>::Add(T data) -> ListNode<T>* {
    return new Cons<T>(data, this);
}

template <typename T>
auto Cons<T>::Remove(T data) -> ListNode<T>* {
    if (head_ == data) {
        return this->tail_;
    }
    return new Cons<T>(this->head_, this->tail_->Remove(data));
}

template <typename T>
auto Cons<T>::Length() -> std::size_t {
    return 1 + this->tail_->Length();
}

template <typename T>
auto Cons<T>::Contains(T data) -> bool {
    if (head_ == data) {
        return true;
    }
    return this->tail_->Contains(data);
}

template <typename T>
auto Cons<T>::Get(int index) -> T {
    if (index == 0) {
        return this->head_;
    }
    return this->tail_->Get(index);
}

template <typename T>
auto Cons<T>::GetHead() -> T {
    return this->head_;
}

template <typename T>
auto Cons<T>::GetTail() -> ListNode<T>* {
    return this->tail_;
}

template <typename T>
class Empty : public ListNode<T> {
   public:
    ~Empty() = default;
    auto Add(T data) -> ListNode<T>* override;

    auto Remove(__attribute__((unused)) T data) -> ListNode<T>* override {
        return this;
    }

    auto Length() -> std::size_t override { return 0; }

    auto Contains(__attribute__((unused)) T data) -> bool override {
        return false;
    }

    auto Get(__attribute__((unused)) int index) -> T override {
        throw std::out_of_range("index out of bounds exception");
    }

    auto GetHead() -> T override {
        throw std::invalid_argument("no such element");
    }

    auto GetTail() -> ListNode<T>* override {
        throw std::invalid_argument("no such element");
    }

    auto GetType() -> ListType override { return ListType::EMPTY; }
};

template <typename T>
auto Empty<T>::Add(T data) -> ListNode<T>* {
    return new Cons<T>(data, this);
}

// Holding useful functions to be used
// over `Immutable List` instances.
namespace ListNodeUtils {

// Transforms the given list's elements
// and returns an new immutable list with the
// transformed elements copied to it.
template <typename T, typename U>
auto Map(ListNode<T>* list, std::function<U(T)> mapper) -> ListNode<U>* {
    switch (list->GetType()) {
        case ListType::EMPTY: {
            return new Empty<U>();
        }
        case ListType::CONS: {
            auto* cons = static_cast<Cons<T>*>(list);
            U newHead = mapper(cons->GetHead());
            ListNode<U>* newTail = Map(cons->GetTail(), mapper);
            return new Cons<U>(newHead, newTail);
        }
        default:
            throw std::invalid_argument("unsupported list type");
    }
}

// Return a list of all the elements matching
// the given `predicate`.
template <typename T>
auto Filter(ListNode<T>* list, std::function<bool(T)> predicate)
    -> ListNode<T>* {
    switch (list->GetType()) {
        case ListType::EMPTY: {
            return new Empty<T>();
        }
        case ListType::CONS: {
            auto* cons = static_cast<Cons<T>*>(list);
            if (!predicate(cons->GetHead())) {
                return Filter(cons->GetTail(), predicate);
            }
            return new Cons<T>(cons->GetHead(),
                               Filter(cons->GetTail(), predicate));
        }
        default:
            throw std::invalid_argument("unsupported list type");
    }
}
}  // namespace ListNodeUtils

#endif
