/**
 * File: list_node.hpp
 * Description: holder for list node implementation
 * Created at: 2018-4-12
 */

#ifndef _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_LIST_LIST_NODE_HPP
#define _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_LIST_LIST_NODE_HPP

template <typename T>
class ListNode {
   public:
    explicit ListNode(T data);
    ListNode(T data, ListNode<T>* prev);
    ListNode(T data, ListNode<T>* prev, ListNode<T>* next);

    T data;
    ListNode<T>* prev = nullptr;
    ListNode<T>* next = nullptr;
};

template <typename T>
ListNode<T>::ListNode(T data) : data(data) {}

template <typename T>
ListNode<T>::ListNode(T data, ListNode<T>* prev) : data(data), prev(prev) {}

template <typename T>
ListNode<T>::ListNode(T data, ListNode<T>* prev, ListNode<T>* next)
    : data(data), prev(prev), next(next) {}

#endif  // _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_LIST_LIST_NODE_HPP
