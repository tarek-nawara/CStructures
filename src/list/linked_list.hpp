/**
 * File: linked_list.hpp
 * Description: interface and implementation for linked list
 * Created at: 2018-4-12
 */

#ifndef _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_LIST_LINKED_LIST_HPP
#define _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_LIST_LINKED_LIST_HPP

#include <functional>
#include <iostream>

#include "list_node.hpp"

template <typename T>
class LinkedList {
   public:
    LinkedList();
    LinkedList(std::initializer_list<T> init_list);
    LinkedList(const LinkedList<T>& list);
    ~LinkedList();
    void Push(T elem);
    auto Pop() -> T;
    auto Back() -> T&;
    auto Get(size_t index) const -> T;
    auto operator[](size_t index) -> T&;
    void RemoveAt(size_t index);
    void Remove(const T& data, std::function<bool(const T&, const T&)>& comp);
    void Reverse();
    [[nodiscard]] auto Size() const -> std::size_t;
    [[nodiscard]] auto Empty() const -> bool;

    template <typename U>
    auto Map(std::function<U(const T&)>& mapper) -> LinkedList<U>;

    template <typename U>
    void MapInPlace(std::function<U(const T&)>& mapper);

    auto Filter(std::function<bool(const T&)>& predicate) -> LinkedList<T>;

    void FilterInPlace(std::function<bool(const T&)>& predicate);

    template <typename FT>
    friend auto operator<<(std::ostream& os, LinkedList<FT> list)
        -> std::ostream&;

   private:
    auto GetNode(size_t index) const -> ListNode<T>*;
    static void RemoveNode(ListNode<T>* node);

    ListNode<T>* head_ = nullptr;
    ListNode<T>* tail_ = nullptr;
    size_t size_ = 0;
};

template <typename T>
LinkedList<T>::LinkedList() = default;

template <typename T>
LinkedList<T>::LinkedList(std::initializer_list<T> init_list) {
    for (auto elem : init_list) {
        this->Push(elem);
    }
}

template <typename T>
LinkedList<T>::LinkedList(const LinkedList<T>& list) {
    for (size_t i = 0; i < list.Size(); ++i) {
        this->Push(list.Get(i));
    }
}

template <typename T>
LinkedList<T>::~LinkedList() {
    auto cur = head_;
    while (cur != nullptr) {
        auto old = cur;
        cur = cur->next;
        delete old;
    }
}

template <typename T>
void LinkedList<T>::Push(T elem) {
    auto node = new ListNode<T>(elem, tail_);
    if (head_ == nullptr) {
        head_ = node;
        tail_ = node;
    } else {
        tail_->next = node;
        tail_ = node;
    }
    ++size_;
}

template <typename T>
auto LinkedList<T>::Pop() -> T {
    if (tail_ == nullptr) {
        throw std::invalid_argument("list is empty");
    }
    auto cur_tail = tail_;
    tail_ = tail_->prev;
    tail_->next = nullptr;
    cur_tail->prev = nullptr;
    auto res = cur_tail->data;
    delete cur_tail;
    --size_;
    return res;
}

template <typename T>
auto LinkedList<T>::Back() -> T& {
    if (tail_ == nullptr) {
        throw std::invalid_argument("list is empty");
    }
    return tail_->data;
}

template <typename T>
auto LinkedList<T>::Get(const size_t index) const -> T {
    return GetNode(index)->data;
}

template <typename T>
auto LinkedList<T>::operator[](const size_t index) -> T& {
    return GetNode(index)->data;
}

template <typename T>
void LinkedList<T>::RemoveAt(const size_t index) {
    if (index >= size_) {
        throw std::invalid_argument("index out of bounds");
    }
    if (index == size_ - 1) {
        this->Pop();
        return;
    }
    auto node = GetNode(index);
    node->prev->next = node->next;
    node->next->prev = node->prev;
    delete node;
    --size_;
}

template <typename T>
void LinkedList<T>::Remove(const T& data,
                           std::function<bool(const T&, const T&)>& comp) {
    auto cur = head_;
    while (cur != nullptr) {
        if (comp(cur->data, data)) {
            if (cur == head_) {
                head_ = cur->next;
            } else if (cur == tail_) {
                tail_ = cur->prev;
            }
            RemoveNode(cur);
            --size_;
            break;
        }
        cur = cur->next;
    }
}

template <typename T>
void LinkedList<T>::Reverse() {
    if (head_ == nullptr || head_->next == nullptr) {
        return;
    }
    auto cur = head_;
    auto nxt = head_->next;
    head_->next = nullptr;
    tail_ = head_;
    while (nxt != nullptr) {
        cur->next = cur->prev;
        cur->prev = nxt;
        cur = nxt;
        nxt = nxt->next;
    }
    cur->next = cur->prev;
    cur->prev = nullptr;
    head_ = cur;
}

template <typename T>
auto LinkedList<T>::Size() const -> std::size_t {
    return size_;
}

template <typename T>
auto LinkedList<T>::Empty() const -> bool {
    return size_ == 0;
}

template <typename T>
template <typename U>
auto LinkedList<T>::Map(std::function<U(const T&)>& mapper) -> LinkedList<U> {
    LinkedList<U> res = *this;
    res.MapInPlace(mapper);
    return res;
}

template <typename T>
template <typename U>
void LinkedList<T>::MapInPlace(std::function<U(const T&)>& mapper) {
    auto cur = head_;
    while (cur != nullptr) {
        cur->data = mapper(cur->data);
        cur = cur->next;
    }
}

template <typename T>
auto LinkedList<T>::Filter(std::function<bool(const T&)>& predicate)
    -> LinkedList<T> {
    LinkedList<T> res = *this;
    res.FilterInPlace(predicate);
    return res;
}

template <typename T>
void LinkedList<T>::FilterInPlace(std::function<bool(const T&)>& predicate) {
    auto cur = head_;
    while (cur != nullptr) {
        auto p = predicate(cur->data);
        auto old = cur;
        cur = cur->next;
        if (!p) {
            if (old->prev == nullptr) {
                head_ = cur;
            }
            RemoveNode(old);
            --size_;
        } else {
            tail_ = old;
        }
    }
    if (size_ == 0) {
        head_ = nullptr;
        tail_ = nullptr;
    }
}

template <typename FT>
auto operator<<(std::ostream& os, LinkedList<FT> list) -> std::ostream& {
    os << '[';
    for (size_t i = 0; i < list.Size(); ++i) {
        if (i > 0) {
            os << ',';
        }
        os << list[i];
    }
    os << ']';
    return os;
}

template <typename T>
auto LinkedList<T>::GetNode(const size_t index) const -> ListNode<T>* {
    if (index >= size_) {
        throw std::invalid_argument("index out of bounds");
    }
    auto cur = head_;
    for (size_t i = 0; i < index; ++i) {
        cur = cur->next;
    }
    return cur;
}

template <typename T>
void LinkedList<T>::RemoveNode(ListNode<T>* node) {
    if (node == nullptr) {
        return;
    }
    node->next->prev = node->prev;
    if (node->prev != nullptr) {
        node->prev->next = node->next;
    }
}

#endif  // _HOME_TAREK_WORKSPACE_CPP_CSTRUCTURES_SRC_LIST_LINKED_LIST_HPP
