/**
 * File: stack.hpp
 * Description: holder for implementation of stack packed by linked list.
 * Created at: 2018-4-13
 */

#ifndef CSTRUCTURES_STACK_HPP
#define CSTRUCTURES_STACK_HPP

#include "../list/linked_list.hpp"

template <typename T>
class Stack {
   public:
    Stack();
    Stack(std::initializer_list<T> init_list);
    ~Stack();
    void Push(T elem);
    T Pop();
    T& Top();
    std::size_t Size();
    T& operator[](size_t index);

   private:
    LinkedList<T> list_;
};

template <typename T>
Stack<T>::~Stack() = default;

template <typename T>
Stack<T>::Stack() = default;

template <typename T>
void Stack<T>::Push(const T elem) {
    list_.Push(elem);
}

template <typename T>
T Stack<T>::Pop() {
    if (list_.Empty()) {
        throw std::invalid_argument("stack is empty");
    }
    auto res = list_.Get(list_.Size() - 1);
    list_.RemoveAt(list_.Size() - 1);
    return res;
}

template <typename T>
T& Stack<T>::Top() {
    return list_.Get(0);
}

template <typename T>
size_t Stack<T>::Size() {
    return list_.Size();
}

template <typename T>
T& Stack<T>::operator[](size_t index) {
    return list_[index];
}

template <typename T>
Stack<T>::Stack(std::initializer_list<T> init_list) : list_(init_list) {}

#endif  // CSTRUCTURES_STACK_HPP
