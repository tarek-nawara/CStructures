/*
 * File: tree_set.hpp
 * Description: holder for implementation of ordered set
 * Created at: 2018-4-18
 */

#ifndef _TREE_SET_HPP_
#define _TREE_SET_HPP_

#include <functional>
#include <vector>

#include "../bst/bst.hpp"

template <typename T>
class TreeSet : public Bst<T, bool> {
   public:
    TreeSet(std::initializer_list<T> init_list);

    TreeSet(const TreeSet<T>& set);

    template <typename V>
    explicit TreeSet(const Bst<T, V>& tree);

    void Add(T data);

    std::vector<T> ToVector();
};

template <typename T>
TreeSet<T>::TreeSet(std::initializer_list<T> init_list) {
    for (auto i : init_list) {
        Add(i);
    }
}

template <typename T>
TreeSet<T>::TreeSet(const TreeSet<T>& set) {
    auto other_vector = set.ToVector();
    for (auto& i : other_vector) {
        Add(i);
    }
}

template <typename T>
template <typename V>
TreeSet<T>::TreeSet(const Bst<T, V>& tree) {
    auto other_vector = tree.EntrySet();
    for (auto& i : other_vector) {
        Add(i.first);
    }
}

template <typename T>
void TreeSet<T>::Add(T data) {
    Bst<T, bool>::Add(data, true);
}

template <typename T>
std::vector<T> TreeSet<T>::ToVector() {
    auto super_vector = Bst<T, bool>::EntrySet();
    std::vector<T> res;
    for (auto& i : super_vector) {
        res.push_back(i.first);
    }
    return res;
}

#endif  // _TREE_SET_HPP_