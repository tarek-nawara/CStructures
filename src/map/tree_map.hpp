/**
 * File: ordered_map.hpp
 * Description: implementation of ordered map using binary search tree
 * Created at: 2018-4-16
 */

#ifndef _TREE_MAP_HPP_
#define _TREE_MAP_HPP_

#include <functional>
#include <utility>

#include "../bst/bst.hpp"

template <typename K, typename V>
class TreeMap : public Bst<K, V> {
   public:
    explicit TreeMap(typename Bst<K, V>::Comp& comp = nullptr)
        : Bst<K, V>(comp) {}
    explicit TreeMap(typename Bst<K, V>::InitList init_list)
        : Bst<K, V>(init_list) {}

    TreeMap(typename Bst<K, V>::Comp& comp,
            typename Bst<K, V>::InitList init_list)
        : Bst<K, V>(comp, init_list) {}

    TreeMap(const TreeMap<K, V>& tree_map) : Bst<K, V>(tree_map) {}
};

#endif  // _TREE_MAP_HPP_
